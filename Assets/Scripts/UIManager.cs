using System.Collections;
using System.Collections.Generic;
using Agora_RTC_Plugin;
using Agora.Rtc;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
using UnityEngine.Android;
#endif

public class UIManager : MonoBehaviour
{
    #region Variable

    public Button leaveButton;
    public Button joinButton;
    public GameObject localViewObject;
    public GameObject remoteViewObject;
    public Slider volumeSlider;
    public TextMeshProUGUI shareScreenText;
    public Toggle muteToggle;

    private bool isSharingScreen = false;

    private string _channelId;
    private ulong _uid;
    


    // Fill in your app ID.
    [SerializeField] private string _appID = "";

// Fill in your channel name.
    [SerializeField] private string _channelName = "";

// Fill in the temporary token you obtained from Agora Console.
    [SerializeField] private string _token = "";

// A variable to save the remote user uid.
    private uint remoteUid;
    internal VideoSurface LocalView;
    internal VideoSurface ShareView;
    internal VideoSurface RemoteView;
    internal IRtcEngine RtcEngine;

#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
    private ArrayList permissionList = new ArrayList() { Permission.Camera, Permission.Microphone };
#endif

    #endregion

    #region UnityMethod

    // Start is called before the first frame update
    void Start()
    {
        SetupVideoSDKEngine();
        InitEventHandler();
        SetupUI();
    }

    // Update is called once per frame
    void Update()
    {
        CheckPermissions();
    }

    #endregion

    #region Method

    private void SetupUI()
    {
        localViewObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180f));
        remoteViewObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180f));

        LocalView = localViewObject.AddComponent<VideoSurface>();
        RemoteView = remoteViewObject.AddComponent<VideoSurface>();

        leaveButton.onClick.AddListener(Leave);
        joinButton.onClick.AddListener(Join);

        shareScreenText.fontSize = 14;
        shareScreenText.GetComponentInParent<Button>().onClick.AddListener(ShareScreen);

        volumeSlider.maxValue = 100;
        volumeSlider.onValueChanged.AddListener(delegate { ChangeVolume((int)volumeSlider.value); });

        muteToggle.isOn = false;
        muteToggle.onValueChanged.AddListener(MuteRemoteAudio);
    }

    private void ShareScreen()
    {
        if (ShareView == null)
        {
            var go = new GameObject("ShareView",  typeof(RawImage));
            go.transform.SetParent(localViewObject.transform);
            var rt = go.GetComponent<RectTransform>();
            rt.pivot = new Vector2(0.5f, 0.5f);
            rt.anchoredPosition = Vector2.zero;
            rt.sizeDelta = new Vector2(0f, 0f);
            rt.anchorMin = new Vector2(0, 0);
            rt.anchorMax = new Vector2(1, 1);
            
            rt.SetParent(transform);
            rt.rotation = Quaternion.Euler(new Vector3(0, 0, 180f));
            ShareView = go.AddComponent<VideoSurface>();
            ShareView.SetEnable(true);
            ShareView.SetForUser(0, "", VIDEO_SOURCE_TYPE.VIDEO_SOURCE_SCREEN);
        }
        if (!isSharingScreen)
        {
            // The target size of the screen or window thumbnail (the width and height are in pixels).
            SIZE t = new SIZE(360, 240);
            // The target size of the icon corresponding to the application program (the width and height are in pixels)
            SIZE s = new SIZE(360, 240);
            // Get a list of shareable screens and windows
            var info = RtcEngine.GetScreenCaptureSources(t, s, true);
            // Get the first source id to share the whole screen.
            Debug.Log("In Custom Function id : ");
            foreach (var source in info)
            {
                var j = $"id : {source.sourceId}\nname : {source.sourceName}\ntitle : {source.sourceTitle}\npath : {source.processPath}\nisPrimary : {source.primaryMonitor}";
                Debug.Log(j);
            }
            var dispId = info[0].sourceId;
            // To share a part of the screen, specify the screen width and size using the Rectangle class.
            // var i = RtcEngine.StartScreenCaptureByWindowId(
            var i = RtcEngine.StartScreenCaptureByDisplayId(
                (uint)dispId, default,
                default(ScreenCaptureParameters));
            Debug.Log(i);
            // Publish the screen track and unpublish the local video track.
            UpdateChannelPublishOptions(true);
            // Display the screen track in the local view.
            SetupLocalVideo(true);
            // Change the screen sharing button text.
            shareScreenText.text = "Stop Sharing";
            // Update the screen sharing state.
            isSharingScreen = true;
        }
        else
        {
            // Stop sharing.
            RtcEngine.StopScreenCapture();
            // Publish the local video track when you stop sharing your screen.
            UpdateChannelPublishOptions(false);
            // Display the local video in the local view.
            SetupLocalVideo(false);
            // Update the screen sharing state.
            isSharingScreen = false;
            // Change to the default text of the button when you stop sharing your screen.
            shareScreenText.text = "Share Screen";
            LocalView.SetForUser(0, "", VIDEO_SOURCE_TYPE.VIDEO_SOURCE_CAMERA);
        }
    }


    private void ChangeVolume(int volume)
    {
        RtcEngine.AdjustRecordingSignalVolume(volume);
    }

    private void MuteRemoteAudio(bool isMute)
    {
    }

    private void Join()
    {
        RtcEngine.EnableVideo();
        // Set the user role as broadcaster.
        RtcEngine.SetClientRole(CLIENT_ROLE_TYPE.CLIENT_ROLE_BROADCASTER);
        // Set the local video view.
        LocalView.SetForUser(0, "", VIDEO_SOURCE_TYPE.VIDEO_SOURCE_CAMERA);
        // Start rendering local video.
        LocalView.SetEnable(true);
        // Join a channel.
        RtcEngine.JoinChannel(_token, _channelName);
    }

    private void Leave()
    {
        // Leaves the channel.
        RtcEngine.LeaveChannel();
        // Disable the video modules.
        RtcEngine.DisableVideo();
        // Stops rendering the remote video.
        RemoteView.SetEnable(false);
        // Stops rendering the local video.
        LocalView.SetEnable(false);
    }

    private void CheckPermissions()
    {
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
        foreach (string permission in permissionList)
        {
            if (!Permission.HasUserAuthorizedPermission(permission))
            {
                Permission.RequestUserPermission(permission);
            }
        }
#endif
    }

    private void SetupVideoSDKEngine()
    {
        // Create an instance of the video SDK.
        RtcEngine = Agora.Rtc.RtcEngine.CreateAgoraRtcEngine();
        // Specify the context configuration to initialize the created instance.
        RtcEngineContext context = new RtcEngineContext(_appID, 0,
            CHANNEL_PROFILE_TYPE.CHANNEL_PROFILE_COMMUNICATION, AUDIO_SCENARIO_TYPE.AUDIO_SCENARIO_DEFAULT,
            AREA_CODE.AREA_CODE_AS);
        // Initialize the instance.
        RtcEngine.Initialize(context);
    }

    private void InitEventHandler()
    {
        // Creates a UserEventHandler instance.
        UserEventHandler handler = new UserEventHandler(this);
        RtcEngine.InitEventHandler(handler);
    }

    void OnApplicationQuit()
    {
        if (RtcEngine != null)
        {
            Leave();
            RtcEngine.Dispose();
            RtcEngine = null;
        }
    }

    private void UpdateChannelPublishOptions(bool publishMediaPlayer)
    {
        ChannelMediaOptions channelOptions = new ChannelMediaOptions();
        channelOptions.publishScreenTrack.SetValue(publishMediaPlayer);
        channelOptions.publishCustomAudioTrack.SetValue(true);
        channelOptions.publishSecondaryScreenTrack.SetValue(publishMediaPlayer);
        channelOptions.publishCameraTrack.SetValue(!publishMediaPlayer);
        RtcEngine.UpdateChannelMediaOptions(channelOptions);
    }

    private void SetupLocalVideo(bool isScreenSharing)
    {
        Debug.Log( shareScreenText.text + isScreenSharing);
        
        // LocalView.SetEnable(!isScreenSharing);
        // LocalView.gameObject.SetActive(isSharingScreen);
        
        // ShareView.SetEnable(isScreenSharing);
        // ShareView.gameObject.SetActive(!isSharingScreen);
        // ShareView.SetForUser(0, "", VIDEO_SOURCE_TYPE.VIDEO_SOURCE_SCREEN);
    }

    #endregion

    #region InternalClass

    internal class UserEventHandler : IRtcEngineEventHandler
    {
        private readonly UIManager _videoSample;

        internal UserEventHandler(UIManager videoSample)
        {
            _videoSample = videoSample;
        }

        // This callback is triggered when the local user joins the channel.
        public override void OnJoinChannelSuccess(RtcConnection connection, int elapsed)
        {
            Debug.Log("You joined channel: " + connection.channelId);
            _videoSample._uid = connection.localUid;
            _videoSample._channelId = connection.channelId;
            
        }

        public override void OnUserJoined(RtcConnection connection, uint uid, int elapsed)
        {
            // Setup remote view.
            _videoSample.RemoteView.SetForUser(uid, connection.channelId, VIDEO_SOURCE_TYPE.VIDEO_SOURCE_REMOTE);
            // Save the remote user ID in a variable.
            _videoSample.remoteUid = uid;
        }

        // This callback is triggered when a remote user leaves the channel or drops offline.
        public override void OnUserOffline(RtcConnection connection, uint uid, USER_OFFLINE_REASON_TYPE reason)
        {
            _videoSample.RemoteView.SetEnable(false);
        }
    }

    #endregion
}